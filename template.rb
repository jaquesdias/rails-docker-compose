def source_paths
  [__dir__]
end

gem 'rswag'
gem 'roo'
gem 'fast_jsonapi'
gem 'bcrypt', '~> 3.1.7'
gem 'knock'
gem 'rack-cors', require: 'rack/cors'

gem_group :development, :test do
  gem 'rspec-rails'
  gem 'shoulda-matchers'
  gem 'rails-controller-testing'
  gem 'pry'
  gem 'pry-byebug'
  gem 'factory_bot_rails'
  gem 'faker', git: 'https://github.com/stympy/faker.git', branch: 'master'
end

gem_group :development do
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
end

after_bundle do
  generate 'rspec:install'
  generate 'rswag:install'
  generate 'knock:install'
  generate 'knock:token_controller user'
end
