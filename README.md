# rails-docker-compose

Prepare a brand new Rails app using docker compose.

[Quickstart: Compose and Rails](https://docs.docker.com/compose/rails/)

Getting started
------------
Move the files to the repository you want to create your rails app:

`Dockerfile`

`Gemfile`

`Gemfile.lock` (must be empty)

`entrypoint.sh`

`docker-compose.yml`

`template.rb`

Don't forget to change 'myapp' on those files to your app name

Build the project
------------
Generate the Rails skeleton app: `docker-compose run web rails new . --force --no-deps --database=postgresql -T -m template.rb`

Build the image again: `docker-compose build`

Connect the database
------------
Go to your `config/database.yml` file and add this to you default config:
```
host: db
username: postgres
password:
```
Create the database running: `docker-compose run web rake db:create`

Boot the application
------------
Run: `docker-compose up` and go to `http://localhost:3000`
